class BangunDatar {
    constructor(nama){
        this._nama = nama;
    }
    
    luas(){
        return "";
    }

    keliling(){
        return "";
    }
}

class Lingkaran extends BangunDatar {
    constructor(nama, jariJari){
        super(nama);
        this._jariJari = jariJari;
    }

    luas(){
        return  Math.PI * this._jariJari * this._jariJari;
    }

    keliling(){
        return  2 * Math.PI * this._jariJari;
    }

    get luasLing(){
        this.luas();
        return this.luas;
    }

    set luasLing(jariJari){
        this._jariJari = jariJari;
    }

    get kelilingLing(){
        this.keliling()
        return this.keliling;
    }

    set kelilingLing(jariJari){
        this._jariJari = jariJari;
    }
}

class Persegi extends BangunDatar {
    constructor(nama, sisi){
        super(nama);
        this._sisi = sisi;
    }

    luas(){
        return this._sisi * this._sisi;
    }

    keliling(){
        return  4 * this._sisi;
    }

    get luasPers(){
        this.luas();
        return this.luas;
    }

    set luasPers(sisi){
        this._sisi = sisi;
    }

    get kelilingPers(){
        this.keliling()
        return this.keliling;
    }

    set kelilingPers(sisi){
        this._sisi = sisi;
    }
}


myLingkaran = new Lingkaran ("lingkaran", 20)
myLingkaran.kelilingLing = 20;
console.log(myLingkaran.kelilingLing);
myLingkaran.luasLing = 15;
console.log(myLingkaran.luasLing);

myPersegi = new Persegi ("Persegi", 30);
myPersegi.kelilingPers = 20;
console.log(myPersegi.kelilingPers);
myPersegi.luasPers = 15;
console.log(myPersegi.luasPers);

