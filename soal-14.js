const  volumeBalok = (panjang, lebar, tinggi) => {
    return panjang * lebar * tinggi;
}

const volumeKubus = (sisi) => {
    return sisi * sisi * sisi;
}

console.log(`
Volume Balok: ${volumeBalok(6,7,8)}cm
Volume Kubus: ${volumeKubus(10)}cm`);